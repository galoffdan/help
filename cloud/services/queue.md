# Очереди сообщений (Message Queue)

Очередь сообщений позволяет запускать обработку данных в отдельных процессах - воркерах. 

В этой статье мы разберем процесс подключения [Yandex Message Queue](https://cloud.yandex.ru/docs/message-queue/)
к [Celery](https://docs.celeryq.dev/en/stable/) и [Django](https://www.djangoproject.com/). 
Запуск воркеров будет происходить в [Serverless Container](/cloud/serverless/container.html).

<iframe width="560" height="315" src="https://www.youtube.com/embed/IQsD7dDuOTw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

[Презентация из видео](https://docs.google.com/presentation/d/13TJomlibdEwQ6yDUzO_ymbdTYP3TRvF3vWM2wMTE-yc/edit)

::: tip Установите CLI
Перед началом <a :href="$page.YANDEX_CLI_HELP" target="_blank">установите Yandex Cloud CLI</a>
:::

## Настройка и запуск воркера

[Установите и настройте модуль `celery-yandex-serverless`](https://github.com/atnartur/celery-yandex-serverless).

[Создайте статический ключ доступа](/cloud/services/service-accounts/iam-key.html)

## Автоматизация

::: tip
[Сначала выполните шаги в инструкции по CI/CD](/cloud/serverless/ci-cd.html)
:::

1. Добавьте [переменные окружения](/deploy/env.html) в Gitlab CI Variables. Описание переменных [здесь](https://github.com/atnartur/celery-yandex-serverless).
  - `YANDEX_MESSAGE_QUEUE_ARN`
  - `CELERY_YANDEX_SERVERLESS_KEY`
  - `AWS_ACCESS_KEY_ID`
  - `AWS_SECRET_ACCESS_KEY`
  - `AWS_DEFAULT_REGION`
  - `CELERY_BROKER_URL=sqs://message-queue.api.cloud.yandex.net:443`
  - `CELERY_BROKER_IS_SECURE=true`
2. В `.gitlab-ci.yml` в конце блока `script` добавьте новую команду:

```yml
  script:
    # (другие команды)
    # пересоздаем триггер
    - yc serverless trigger delete --name celery > /dev/null || echo
    - >
      yc serverless trigger create message-queue
      --name celery
      --queue $YANDEX_MESSAGE_QUEUE_ARN
      --queue-service-account-name $SERVICE_ACCOUNT_NAME
      --service-account-id $SERVICE_ACCOUNT_ID
      --invoke-container-service-account-id $SERVICE_ACCOUNT_ID
      --invoke-container-path /worker/$CELERY_YANDEX_SERVERLESS_KEY
      --batch-size 1
      --batch-cutoff 10s
```

## Запуск по расписанию (cron)

Бывает необходимость запускать операции в приложении по расписанию в определенное время (например, раз в день нужно сделать синхронизацию данных в внешней системой). В Linux серверах это делается с помощью `cron`. 

В Celery есть `celery-beat`, делающий то же самое. Но в serverless-развертывании воркер как таковой не запускается, поэтому нужно настраивать запуск по расписанию иначе.

Эту задачу поможет решить **триггер "Таймер"**. О нем написано в [документации Яндекс.Облака](https://cloud.yandex.ru/docs/serverless-containers/concepts/trigger/timer). Создать его можно как через интерфейс, так и через консоль.

Написать **cron выражение** для триггера поможет сайт [crontab.guru](http://crontab.guru).

## Источники

- [Подключение Celery к Yandex Message Queue - Yandex Cloud Docs](https://cloud.yandex.ru/docs/message-queue/instruments/celery)
- [Развертывание Django+Celery приложения в Яндекс.Облаке с использованием Serverless технологий - atnartur.ru](https://atnartur.ru/posts/2022/celery-in-yandex-cloud/)
  