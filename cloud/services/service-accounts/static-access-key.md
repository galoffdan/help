# Создание статического ключа доступа (для AWS API)

::: tip 
Статический ключ доступа доступа IAM (IAM access key) нужен для аутентификации в Yandex Object Storage API и других API, совместимых с Amazon Web Services [Подробнее](https://cloud.yandex.ru/docs/iam/concepts/authorization/access-key).
:::

::: warning Установите CLI
Перед продложением работы <a :href="$page.YANDEX_CLI_HELP" target="_blank">установите Yandex Cloud CLI</a>
:::

1. Перейдите на страницу каталога в Яндекс.Облаке
2. Зайдите в раздел **Сервисные аккаунты**
3. Посмотрите название сервисного аккаунта в каталоге Яндекс.Облака
4. Сгенерируйте `ACCESS_KEY` и `SECRET_KEY` с помощью команды 
(замените `SERVICE_ACCOUNT_NAME` на название сервисного аккаунта):

```bash
yc iam access-key create --service-account-name SERVICE_ACCOUNT_NAME
```

Команда вернет access key и secret key. Они позволят подключиться к сервисам Яндекс.Облака.

```yml{5,6}
access_key:
  id: aje...
  service_account_id: aje...
  created_at: "2023-03-24T17:49:01.555836400Z"
  key_id: YCAJ... # <- Это access key
secret: YCPM... # <- Это secret key
```

Для того, чтобы передать эти ключи в приложение, нужно установить [переменные окружения](/deploy/env.html):

```
AWS_ACCESS_KEY_ID="access key, скопированный выше"
AWS_SECRET_ACCESS_KEY="secret key, скопированный выше"
AWS_DEFAULT_REGION="ru-central1"
```