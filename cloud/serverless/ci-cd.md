# 5. Автоматическое развертывание (CI/CD)

Автоматическое развертывание в Яндекс.Облако описано для [Gitlab CI](https://gitlab.com). 

::: tip Установите CLI
Перед началом <a :href="$page.YANDEX_CLI_HELP" target="_blank">установите Yandex Cloud CLI</a>
:::

::: warning Копируйте примеры кода из документации
Некоторые детали в видео могли устареть
:::

<iframe width="560" height="315" src="https://www.youtube.com/embed/MNzpcI33IT0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Создание ключа сервисного аккаунта

Первым делом нужно настроить возможность выполнять действия в облаке из CI. [**Создайте авторизованный ключ по инструкции**](/cloud/services/service-accounts/iam-key.html).

## Установка переменных окружения в Gitlab CI Variables

Откройте проект в Gitlab, перейдите в Settings / CI/CD / Variables и добавьте переменные:
1. `YANDEX_CLOUD_KEY` с содержимым из файла `key.json`. Установите тип переменной `file`

![](../../images/cloud/serverless/env-file-var.jpg)

2. `YANDEX_CLOUD_FOLDER_ID` - впишите ID облака ([где взять ID облака](https://cloud.yandex.ru/docs/resource-manager/operations/folder/get-id))
   
![](../../images/cloud/serverless/env-usual-var.jpg)

3. `YANDEX_CLOUD_REGISTRY` - впишите адрес registry (например, `cr.yandex/adjkfasjfhjksadf`)
4. `YANDEX_CONTAINER_NAME` - впишите название serverless container, [созданного ранее](/cloud/serverless/container.html)
5. Установите другие переменные окружения, необходимые для проекта
   - например, для подключения к БД (`DB_NAME`, `DB_USER`, `DB_HOST`, `DB_PASSWORD`)

## Убрать запуск миграции из Dockerfile

Ранее запуск миграций был поставлен в `CMD` в `Dockerfile`, чтобы они запускались при старте контейнера.

В связи с тем, что сейчас появляется автоматический способ развертывания, нужно миграции перенести в CI и 
убрать их из контейнера. Таким образом, контейнер будет запускаться быстрее.

Уберите команду миграции из `CMD` в `Dockerfile`, далее мы добавим ее в CI.

## Настройка конфигурации CI

В коде проекта создайте файл `.gitlab-ci.yml`. В шаблоне есть комментарии, сделайте замены, если это необходимо.

```yml
stages:
  - deploy

deploy_cloud:
  image: atnartur/yc:latest
  stage: deploy
  script:
    # устанавливаем ключ сервисного аккаунта
    - yc config set service-account-key $(echo $YANDEX_CLOUD_KEY)
    # устанавливаем ID каталога
    - yc config set folder-id $YANDEX_CLOUD_FOLDER_ID
    # настраиваем подключение к Yandex Cloud Registry
    - yc container registry configure-docker
    # собираем образ 
    # (поменяйте путь до докерфайла после флага -f, если это необходимо)
    - docker build -t $YANDEX_CLOUD_REGISTRY/app:latest -f Dockerfile .
    # отправляем образ в registry
    - docker push $YANDEX_CLOUD_REGISTRY/app:latest
    # вытаскиваем service account id из ключа
    - export SERVICE_ACCOUNT_ID=$(yc config get service-account-key | grep service_account_id | awk '{print $2}')
    # запускаем миграции, чтобы не запускать их при старте контейнера
    - >
      docker run --rm 
      --env DB_NAME=$DB_NAME
      --env DB_USER=$DB_USER
      --env DB_HOST=$DB_HOST
      --env DB_PASSWORD=$DB_PASSWORD
      $YANDEX_CLOUD_REGISTRY/app:latest 
      python manage.py migrate
    # обновляем контейнер и прокидываем переменные окружения с параметрами подключения к БД
    # (они должны быть также установлены в Gitlab CI Variables)
    - >
        yc serverless container revision deploy 
        --image $YANDEX_CLOUD_REGISTRY/app:latest 
        --container-name $YANDEX_CONTAINER_NAME
        --service-account-id $SERVICE_ACCOUNT_ID
        --core-fraction 5
        --execution-timeout 30s
        --environment DB_NAME=$DB_NAME
        --environment DB_USER=$DB_USER
        --environment DB_HOST=$DB_HOST
        --environment DB_PASSWORD=$DB_PASSWORD
        > /dev/null
    # если в приложении есть еще какие-нибудь переменные окружения, прокиньте их здесь
  tags:
    # тег раннера, где будут запускаться сборки 
    # (обычно менять не нужно, но если что, он находится здесь)
    - unienv_shared 
```

## Источники

- [Авторизация в `yc` с помощью сервисного аккаунта](https://cloud.yandex.ru/docs/cli/operations/authentication/service-account)
- Деплой новой версии контейнера: [инструкция](https://cloud.yandex.ru/docs/serverless-containers/operations/manage-revision), [параметры в cli](https://cloud.yandex.ru/docs/cli/cli-ref/managed-services/serverless/container/revision/deploy)