# Развертывание статического сайта в Object Storage

::: tip Перед началом
Изучите [статью про Object Storage](/cloud/services/s3.html)
:::

[Object Storage](/cloud/services/s3.html) может не только хранить загружаемые пользователями файлы, но и служить хостингом для статических сайтов. Статический сайт - любой сайт, который реализован как набор неизменяемых файлов (например, фронтенд на HTML, CSS, JS, картинки...).

## Подготовка

- Откройте бакет 
- Перейдите в раздел **Веб-сайт**
- Включите **Хостинг**
- Укажите главную страницу и страницу ошибки `index.html`
  - _Для того, чтобы все запросы попадали в index.html, который запускает single page application, мы указываем главную страницу и страницу ошибки как `index.html`._
- Сохраните изменения

Позже мы подключим бакет к api gateway, чтобы открыть сайт.

## Загрузка файлов

Загрузить файлы можно [несколькими способами](https://cloud.yandex.ru/docs/storage/tools/). Например, вручную это можно сделать прямо через браузер в консоли Яндекс.Облака. 

Ниже мы рассмотрим процесс загрузки с помощью AWS CLI.

1. Установите [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
2. Установите переменные окружения
```bash
export AWS_ACCESS_KEY_ID=accesskey
export AWS_SECRET_ACCESS_KEY=secretkey
export AWS_DEFAULT_REGION=ru-central1
```

3. Выполните команду, заменив `local_folder` на папку, из которой надо загружать файлы, а `bucket-name` - на название бакета
```bash
aws --endpoint-url=https://storage.yandexcloud.net \
  s3 cp --recursive \
  local_folder/ \ 
  s3://bucket-name/
```

Чтобы автоматизировать развертывание сайта в Object Storage, эти шаги можно перенести в [Gitlab CI](/cloud/serverless/ci-cd.html).


## Подключение к API Gateway

Дополните конфигурацию [API Gateway](/cloud/serverless/api_gateway.html)

```yml{9,12,25,28}
# здесь другие настройки
paths:
  # добавляем новые пути:
  /:  # раздаем index.html
    get:
      summary: Serve static file from Yandex Cloud Object Storage
      x-yc-apigateway-integration:
        type: object_storage
        bucket: bucket_name # вставьте название бакепа
        object: index.html
        error_object: error.html
        service_account_id: ajeo... # вставьте ID сервисного аккаунта
  /{file+}:  # раздаем остальные файлы
    get:
      summary: Serve static file from Yandex Cloud Object Storage
      parameters:
        - name: file
          explode: false
          in: path
          required: true
          schema:
            type: string
      x-yc-apigateway-integration:
        type: object_storage
        bucket: bucket_name # вставьте название бакепа
        object: '{file}'
        error_object: index.html
        service_account_id: ajeo... # вставьте ID сервисного аккаунта
```

## Автоматического развертывания в CI

**Dockerfile**
```Dockerfile
FROM node:16-alpine as node

RUN echo $AWS_DEFAULT_REGION  # проверяем, что переменные видны при сборке

# Установка зависимостей для awscli
RUN apk update && apk add --no-cache curl python3 py3-pip && \
    pip install --upgrade pip && \
    pip install --upgrade awscli

RUN aws --version

# Сборка проекта
WORKDIR /app

COPY package.json ./
COPY yarn.lock ./

RUN yarn install --silent

COPY . .

# Переменные нужно также прокинуть в .gitlab-ci.yml при сборке образа
ARG AWS_ACCESS_KEY_ID
ARG AWS_DEFAULT_REGION
ARG AWS_SECRET_ACCESS_KEY

ARG PROJECT_VAR # здесь можно добавить переменные проекта

RUN yarn build

# Копирование файлов в бакет и удаление загруженных ранее
RUN aws --endpoint-url=https://storage.yandexcloud.net s3 rm --recursive s3://bucket_name # вставьте название бакета
RUN aws --endpoint-url=https://storage.yandexcloud.net s3 cp --recursive build/ s3://bucket_name/ # вставьте название бакета
```

**.gitlab-ci.yml**
```yml
deploy:
  stage: deploy
  script:
    - >
      docker build 
      --build-arg PROJECT_VAR=value 
      --build-arg AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
      --build-arg AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION
      --build-arg AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
      -f Dockerfile -t yc_container .
  tags:
    - unienv_shared
```
