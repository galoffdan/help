# Развертывание на базе собранного Docker образа

1. Откройте страницу репозитория в Gitlab
2. Перейдите в раздел "Packages & Registries / Container Registry". На этой странице будут показаны команды, которые 
помогут выполнить следующие шаги.
3. Авторизуйтесь в Gitlab Container Registry: `docker login registry.gitlab.com`
4. Запустите сборку образа `docker build -t registry.gitlab.com/GROUPNAME/REPONAME .`
5. Загрузите образ в Registry: `docker push registry.gitlab.com/GROUPNAME/REPONAME`
6. Установите [переменную окружения](/deploy/env.html) **`DOCKER_IMAGE`** в значение `GROUPNAME/REPONAME` 
(`registry.gitlab.com/` в начале нужно **убрать**)
7. Запустите [развертывание UniEnv](/). Платформа автоматически подхватит Docker-образ и развернет его.

## Автоматизация с помощью Gitlab CI

Пример конфигурационного файла `.gitlab-ci.yaml` с настроенной сборкой Docker образа

```yaml{12-14}
stages:
  - build
  - deploy

build:
  stage: build
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build -t $CI_REGISTRY_IMAGE .
    - docker push $CI_REGISTRY_IMAGE:latest
  tags:
    # поставьте тег вашего Gitlab Runner, чтобы управлять сервером, 
    # который будет выполнять сборку
    - runner_tag 

deploy:
  stage: deploy
  script:
    # Скрипт сборки указан внутри системы и
    # никаких дополнительных настроек не требуется
    - echo
  tags:
    - a.uenv.ru
    - unienv_deploy
  environment:
    name: UniEnv
    url: http://a.uenv.ru
```
